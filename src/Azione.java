/*************************************************************************
 *  
 * Author: 
 *    Daniele Zambon
 * 
 * License: 
 *    Copyright (c) 2013-2019 and later, Daniele Zambon.
 *    Copyright (c) 2002 and later by MH Software-Entwicklung. 
 *    All Rights Reserved.
 *
 *  This file is part of Stac.
 *
 *  Stac is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Stac is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Stac.  If not, see <http://www.gnu.org/licenses/>.
 *
 ************************************************************************/

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

import javax.swing.JOptionPane;
import javax.swing.JTable;

public class Azione {
	/**
	 * annulla l'operazione in corso
	 */
	public static void btnAnnullaAction(){
		hideSubmenu();
		clear();
		return;
	}
	/**
	 * crea una nuova lista di schede da un file che viene richiesto di inserire,
	 * @throws IOException
	 */
	public static void btnApriAction() throws IOException{
		String s = (String)JOptionPane.showInputDialog(
				mainWin.frame,
                "Inserire nome file da aprire:\n(se non specificato il path è \"./\")",
                "Customized Dialog",
                JOptionPane.PLAIN_MESSAGE
                );
		if ((s != null) && (s.length() > 0)){
			int opt = JOptionPane.showConfirmDialog(
					mainWin.frame,
				    "Sei sicura di voler eliminare il cliente\n"+mainWin.txtCognome.getText().toUpperCase()+" "+mainWin.txtNome.getText().toUpperCase()+" ?",
				    "Elimina",
				    JOptionPane.YES_NO_OPTION);
			if(opt==0)
				mainWin.schede = new Lista(s);
		}
		return;
	}
	/**
	 * cerca un cliente
	 * @param opt 0: cerca per nome; 1: per cognome; 2: per cognome e nome; 3:cerca all'interno sia di nome che di cognome;
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public static void btnCercaAction(int opt) throws UnsupportedEncodingException, FileNotFoundException{
		hideInfo();
		String c,n;
		n=mainWin.txtNome.getText();
		c=mainWin.txtCognome.getText();
		clearTable();
		int i;
		if (opt==0)
			i=mainWin.schede.cercaByN(n);
		else if (opt==1 )
			i=mainWin.schede.cercaByC(c);
		else if (opt ==2)
			i=mainWin.schede.cercaByCN(c,n);
		else if (opt ==3){
			mainWin.txtSfoglia.getText();
			i=mainWin.schede.cercaSimultanea(mainWin.txtSfoglia.getText());
		}
		else
			return;
		
		if (i>=0){
			setCliente(i);
			mainWin.btnInfo.setVisible(true);
		}
		else{
			setCliente(-1);
			mainWin.btnInfo.setVisible(false);
		}
		hideSubmenu();
		return;
	}
	/**
	 * procedure da attuarsi prima di terminare il programma, terminazione programma.
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public static void btnChiudiAction() throws UnsupportedEncodingException, FileNotFoundException{
		mainWin.schede.backup();	
		mainWin.schede.salva();
		Scanner scanner = new Scanner(new File("data/salvataggio-chiavetta.bst"));
		mainWin.schede.salva(scanner.nextLine()+"/chiavetta");
		scanner.close();
		int opt = JOptionPane.showConfirmDialog(
				mainWin.frame,
			    "Sei sicura di voler uscire ?",
			    "Chiudi",
			    JOptionPane.YES_NO_OPTION);
		if(opt==0)
			System.exit(0);
		return;
	}
	/**
	 * conferma inserimento di un nuovo cliente
	 * @throws HeadlessException
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public static void btnConfermaInserimentoAction() throws HeadlessException, UnsupportedEncodingException, FileNotFoundException{
		String c,n,col,per,tri,tel,via,cap,p;
		
		c=mainWin.txtDc.getText();
		n=mainWin.txtDn.getText();
		col=mainWin.txtrDcol.getText();
		per=mainWin.txtrDper.getText();
		tri=mainWin.txtrDtri.getText();
		tel=mainWin.txtTel.getText();
		via=mainWin.txtVia.getText();
		cap=mainWin.txtCap.getText();
		p=mainWin.txtPaese.getText();
			
		Cliente cliente= new Cliente(c,n,col,per,tri,tel,via,cap,p);
		
		int opt = JOptionPane.showConfirmDialog(
				mainWin.frame,
			    "Sei sicura di voler inserire il nuovo cliente:\n"+cliente.toDialog()+" ?",
			    "Inserisci",
			    JOptionPane.YES_NO_OPTION);
		if(opt==0)
			mainWin.schede.newCliente(cliente);

		clearTable();
		mainWin.schede.cercaByCN(c,n);
		
		mainWin.txtDn.setEditable(false);
		mainWin.txtDc.setEditable(false);
		mainWin.txtrDcol.setEditable(false);
		mainWin.txtrDper.setEditable(false);
		mainWin.txtrDtri.setEditable(false);
		mainWin.txtTel.setEditable(false);
		mainWin.txtVia.setEditable(false);
		mainWin.txtCap.setEditable(false);
		mainWin.txtPaese.setEditable(false);
		mainWin.txtId.setVisible(true);

		mainWin.schede.salvataggioAutomatico();
		clear();
		hideInfo();
		hideSubmenu();
		return;
	}
	/**
	 * conferma le modifiche apportate al cliente 
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public static void btnConfermaModificheAction() throws UnsupportedEncodingException, FileNotFoundException{
		String c,n,col,per,tri,tel,via,cap,p;
		int index;
		
		index=Integer.parseInt(mainWin.txtId.getText());
		c=mainWin.txtDc.getText();
		n=mainWin.txtDn.getText();
		col=mainWin.txtrDcol.getText();
		per=mainWin.txtrDper.getText();
		tri=mainWin.txtrDtri.getText();
		tel=mainWin.txtTel.getText();
		via=mainWin.txtVia.getText();
		cap=mainWin.txtCap.getText();
		p=mainWin.txtPaese.getText();

//		int i=mainWin.schede.cercaByCN(c,n);
		
		String modifiche=" * COLORE\n    da:\n"+mainWin.schede.getCliente(index).displayColore()+"\n    a:\n"+col+
				"\n * PERMANENTE\n    da:\n"+mainWin.schede.getCliente(index).displayPermanente()+"\n    a:\n"+per+
				"\n * TRICOLOGICO\n    da:\n"+mainWin.schede.getCliente(index).displayTricologico()+"\n    a:\n"+tri+
				"\n * INFO\n    da:\n"+mainWin.schede.getCliente(index).getTelefono()+"  -  "+
				mainWin.schede.getCliente(index).getVia()+"  -  "+
				mainWin.schede.getCliente(index).getCap()+"  -  "+
				mainWin.schede.getCliente(index).getPaese()+
				"\n    a:\n"+tel+"  -  "+via+"  -  "+cap+"  -  "+p;
				
		int opt = JOptionPane.showConfirmDialog(
				mainWin.frame,
			    "Sei sicura di voler modificiare il cliente\n"+c.toUpperCase()+" "+n.toUpperCase()+"\n"+modifiche+"\n ?",
			    "Modifica",
			    JOptionPane.YES_NO_OPTION);
		if(opt==0){
			mainWin.schede.getCliente(index).setInfo(tel,via,cap,p);
			mainWin.schede.getCliente(index).setTrattamenti(col,per,tri);
			clearTable();
			mainWin.schede.addToTable(0,index);
		}
		
		mainWin.txtDn.setEditable(false);
		mainWin.txtDc.setEditable(false);
		mainWin.txtrDcol.setEditable(false);
		mainWin.txtrDper.setEditable(false);
		mainWin.txtrDtri.setEditable(false);
		mainWin.txtTel.setEditable(false);
		mainWin.txtVia.setEditable(false);
		mainWin.txtCap.setEditable(false);
		mainWin.txtPaese.setEditable(false);	
		mainWin.schede.salvataggioAutomatico();
		clear();
		hideSubmenu();
		hideInfo();
		return;
	}
	/**
	 * elimina il cliente a display
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public static void btnEliminaAction() throws UnsupportedEncodingException, FileNotFoundException{
		int index=Integer.parseInt(mainWin.txtId.getText());
	
//		int i=mainWin.schede.cercaByCN(mainWin.txtDc.getText(),mainWin.txtDn.getText());

		int opt = JOptionPane.showConfirmDialog(
				mainWin.frame,
			    "Sei sicura di voler elimiare il cliente:\n"+mainWin.schede.getCliente(index).toDialog()+" ?",
			    "Elimina",
			    JOptionPane.YES_NO_OPTION);
		if(opt==0){
			mainWin.schede.removeCliente(index);
			clearTable();
		}

		mainWin.schede.salvataggioAutomatico();
		return;
	}
	/**
	 * mostra e nasconde le informazioni "num telefono", "via", "CAP" e "paese"
	 */
	public static void btnInfoAction(){
		if(!mainWin.txtTel.isVisible())
			showInfo();
		else
			hideInfo();
		return;
	}
	/**
	 * dispone il display per poter ricevere gli argomenti dei campi del nuovo cliente
	 */
	public static void btnInserisciAction(){
		clear();
		showInfo();

		mainWin.txtId.setVisible(false);
		mainWin.txtDn.setEditable(true);
		mainWin.txtDc.setEditable(true);
		mainWin.txtrDcol.setEditable(true);
		mainWin.txtrDper.setEditable(true);
		mainWin.txtrDtri.setEditable(true);
		mainWin.txtTel.setEditable(true);
		mainWin.txtVia.setEditable(true);
		mainWin.txtCap.setEditable(true);
		mainWin.txtPaese.setEditable(true);
		hideSubmenu();
		mainWin.btnAnnulla.setVisible(true);
		mainWin.btnConfermaInserimento.setVisible(true);
		return;
	}
	/**
	 * predispone il display per poter modificare i campi del cliente selezionato 
	 */
	public static void btnModificaAction(){
		showInfo();
		mainWin.txtrDcol.setEditable(true);
		mainWin.txtrDper.setEditable(true);
		mainWin.txtrDtri.setEditable(true);
		mainWin.txtTel.setEditable(true);
		mainWin.txtVia.setEditable(true);
		mainWin.txtCap.setEditable(true);
		mainWin.txtPaese.setEditable(true);
		mainWin.btnConfermaModifiche.setVisible(true);
		mainWin.btnAnnulla.setVisible(true);
		return;
	}
	/**
	 * mostra nella tabella tutti i clienti registrati
	 */
	public static void btnSfogliaAction(){
		for(int i=0;i<mainWin.NROWS;i++)
			mainWin.schede.addToTable(i,i);
		return;
	}
	public static void btnTestAction() throws UnsupportedEncodingException, FileNotFoundException{
		//file di log
		Scanner scanner=new Scanner(new File("data/s.bst"));
		scanner.close();
		return;
	}
	/**
	 * pulisce il display dall'ultima operazione effettuata
	 */
	public static void clear(){
		mainWin.txtId.setText("-1");
		mainWin.txtDn.setText("");
		mainWin.txtDc.setText("");
		mainWin.txtrDcol.setText("");
		mainWin.txtrDper.setText("");
		mainWin.txtrDtri.setText("");
		mainWin.txtTel.setText("");
		mainWin.txtVia.setText("");
		mainWin.txtCap.setText("");
		mainWin.txtPaese.setText("");
		mainWin.txtDn.setEditable(false);
		mainWin.txtDc.setEditable(false);
		mainWin.txtrDcol.setEditable(false);
		mainWin.txtrDper.setEditable(false);
		mainWin.txtrDtri.setEditable(false);
		mainWin.txtTel.setEditable(false);
		mainWin.txtVia.setEditable(false);
		mainWin.txtCap.setEditable(false);
		mainWin.txtPaese.setEditable(false);
		hideInfo();
		return;
	}
	/**
	 * pulisce la tabella dall'ultima operazione effettuata
	 */
	public static void clearTable(){
		for(int j=0;j<mainWin.NROWS;j++)
			mainWin.table.setValueAt("-1",j,0);
		for(int i=1;i<=5;i++)
			for(int j=0;j<mainWin.NROWS;j++)
				mainWin.table.setValueAt("",j,i);
		return;
	}
	/**
	 * funzione utilizzata per il debug.
	 * stampa nella area di testo txtrOutput e scrive in append in un file .log
	 * @param msg messaggio da scrivere
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public static void displayMsg(String msg) throws UnsupportedEncodingException, FileNotFoundException{
		//file di log
		PrintWriter pw = new PrintWriter(
				new OutputStreamWriter(
						new FileOutputStream("data/scat.log",true),
						"UTF-8"));
		Calendar oggi = Calendar.getInstance();
		SimpleDateFormat formato = new SimpleDateFormat("yyyyMMddhhmma");
		pw.println(formato.format(oggi.getTime())+">>"+msg);
		pw.close();
		//txtrOutput
		String txt=mainWin.txtrOutput.getText().concat(msg);
		int rows=0,i,j=0;
		final int COLMAX=75,ROWMAX=5;
		for(i=0;i<txt.length();i++){
			j++;
			if(j==COLMAX)
				txt=txt.substring(0, i).concat("\n").concat(txt.substring(i));
			if( txt.charAt(i)=='\n'){ 
				rows++;
				j=0;
			}
		}
		if (rows>ROWMAX){
			i=0;
			for(int k=rows-ROWMAX+1;k>0;i++)
				if( txt.charAt(i)=='\n')
					k--;
			txt=txt.substring(i);
		}
		mainWin.txtrOutput.setText(txt+"\n");
		return;
	}
	/**
	 * 
	 * @return tabella
	 */
	public static JTable getTable(){
		return mainWin.table;
	}
	/**
	 * nasconde le info come "num telefono", "via", "CAP" e "paese"
	 */
	public static void hideInfo(){
		mainWin.txtTel.setVisible(false);
		mainWin.txtVia.setVisible(false);
		mainWin.txtCap.setVisible(false);
		mainWin.txtPaese.setVisible(false);
		return;
	}
	/**
	 * nasconde il sotto-menu composto da bottoni come "confermaModifiche", "annulla"...
	 */
	public static void hideSubmenu(){
		mainWin.btnAnnulla.setVisible(false);
		mainWin.btnConfermaInserimento.setVisible(false);
		mainWin.btnConfermaModifiche.setVisible(false);
		return;
	}
	/**
	 * mostra le informazioni sul programma
	 */
	public static void mnInformazioniAction(){
		//TODO licenza
        JOptionPane.showMessageDialog(mainWin.frame,
			    "NOME PROGRAMMA: stac\n"+
			    "VERSIONE: 1.9\n\n"+
			    "Copyright (c) 2013, Daniele Zambon.\nCopyright (c) 2002 and later by MH Software-Entwicklung.\nAll Rights Reserved.\nThis file is part of Stac.\nStac is free software: you can redistribute it and/or modify\nit under the terms of the GNU General Public License as published by\nthe Free Software Foundation, either version 3 of the License, or\n(at your option) any later version.\nStac is distributed in the hope that it will be useful,\nbut WITHOUT ANY WARRANTY; without even the implied warranty of\nMERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\nGNU General Public License for more details.\nYou should have received a copy of the GNU General Public License\nalong with Stac.  If not, see <http://www.gnu.org/licenses/>.\n\n"+
			    "L&F swing fornito da JTatoo;\n",
			    "Informazioni",
			    JOptionPane.INFORMATION_MESSAGE);
		return;
	}
	/**
	 * mostra le istruzioni per l'utilizzo del programma
	 */
	public static void mnIstruzioniAction(){
		//TODO
        JOptionPane.showMessageDialog(mainWin.frame,
			    "Al momento non ho ancora sviluppato la sezione con la documentazione;",
			    "Istruzioni",
			    JOptionPane.INFORMATION_MESSAGE);
		return;
	}
	/**
	 * imposta il luogo in cui salvare il file chiavetta.bst
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public static void mnSalvataggioChiavettaAction() throws FileNotFoundException, UnsupportedEncodingException{
		Scanner scanner = new Scanner(new File("data/salvataggio-chiavetta.bst"));
		scanner.nextLine();
		String s = (String)JOptionPane.showInputDialog(
				mainWin.frame,
                "il salvataggio per chiavetta è impostato in:\n"+scanner.nextLine()+"\ninserire il nuovo path:",
                "Customized Dialog",
                JOptionPane.PLAIN_MESSAGE
                );
		if ((s != null) && (s.length() > 0)){
			PrintWriter pw = new PrintWriter(
					new OutputStreamWriter(
							new FileOutputStream("data/salvataggio-chiavetta.bst"),
							"UTF-8"));
			pw.printf(s);
			pw.close();
		}		
		scanner.close();
		return;
	}
	/**
	 * salva la lista su file
	 * @param opt 0: salvataggio su file di default; 1: salva su file da specificare
	 */
	public static void salvaAction(int opt){
		try {
			if(opt==0)
				mainWin.schede.salva();
			else if (opt==1){
				String s = (String)JOptionPane.showInputDialog(
						mainWin.frame,
	                    "Inserire nome file:\n(se non specificato il path è \"./\")",
	                    "Customized Dialog",
	                    JOptionPane.PLAIN_MESSAGE
	                    );
				if ((s != null) && (s.length() > 0))
					mainWin.schede.salva(s);
			}
	    return;
		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(mainWin.frame,
				    e,
				    "Error",
				    JOptionPane.ERROR_MESSAGE);
		}
	}
	/**
	 * mostra nel display il cliente selezionato
	 * @param index indice di posizione del cliente nella lista (se -1 imposta cliente nullo)
	 */
	public static void setCliente(int index){
		if (index==-1){
			mainWin.txtId.setText(index+"");
			mainWin.txtDn.setText("NON TROVATO");
			mainWin.txtDc.setText("NON TROVATO");
			mainWin.txtrDcol.setText("CLIENTE NON TROVATO IN ARCHIVIO");
			mainWin.txtrDper.setText("CLIENTE NON TROVATO IN ARCHIVIO");
			mainWin.txtrDtri.setText("CLIENTE NON TROVATO IN ARCHIVIO");
			mainWin.txtTel.setText("");
			mainWin.txtVia.setText("");
			mainWin.txtCap.setText("");
			mainWin.txtPaese.setText("");
		}
		else{
			mainWin.txtId.setText(index+"");
			mainWin.txtDn.setText(mainWin.schede.getCliente(index).getNome());
			mainWin.txtDc.setText(mainWin.schede.getCliente(index).getCognome());
			mainWin.txtrDcol.setText(mainWin.schede.getCliente(index).displayColore(48,5));
			mainWin.txtrDper.setText(mainWin.schede.getCliente(index).displayPermanente(48,4));
			mainWin.txtrDtri.setText(mainWin.schede.getCliente(index).displayTricologico(48,4));
			mainWin.txtTel.setText(mainWin.schede.getCliente(index).getTelefono());
			mainWin.txtVia.setText(mainWin.schede.getCliente(index).getVia());
			mainWin.txtCap.setText(mainWin.schede.getCliente(index).getCap());
			mainWin.txtPaese.setText(mainWin.schede.getCliente(index).getPaese());
		}
		return;
	}
	/**
	 * mostra le info come "num telefono", "via", "CAP" e "paese"
	 */
	public static void showInfo(){
		mainWin.txtTel.setVisible(true);
		mainWin.txtVia.setVisible(true);
		mainWin.txtCap.setVisible(true);
		mainWin.txtPaese.setVisible(true);
		return;
	}
	/**
	 * mostra nel display il cliente selezionato dalla tabella
	 * @param e evento di puntamento del mouse
	 */
	public static void tablesRowAction(java.awt.event.MouseEvent e){
		hideInfo();
		int index=Integer.parseInt( mainWin.table.getValueAt(mainWin.table.rowAtPoint(e.getPoint()),0).toString());
		if (index>=0){
			setCliente(index);
			mainWin.btnInfo.setVisible(true);
		}
		hideSubmenu();
		return;
	}
	/**
	 * in maniera simultanea alla digitazione dei caratteri cerca
	 * all'interno di nomi e cognomi i caratteri inseriti nel textField 
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public static void txtSfogliaAction() throws UnsupportedEncodingException, FileNotFoundException{
		btnCercaAction(3);
		return;
	}
}
