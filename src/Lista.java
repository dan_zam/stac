/*************************************************************************
 *  
 * Author: 
 *    Daniele Zambon
 * 
 * License: 
 *    Copyright (c) 2013-2019 and later, Daniele Zambon.
 *    Copyright (c) 2002 and later by MH Software-Entwicklung. 
 *    All Rights Reserved.
 *
 *  This file is part of Stac.
 *
 *  Stac is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Stac is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Stac.  If not, see <http://www.gnu.org/licenses/>.
 *
 ************************************************************************/

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;

import javax.swing.JOptionPane;
public class Lista {

	private ArrayList <Cliente> listaSchede=new ArrayList<Cliente>();
	private String nomeFile;
	private static boolean update;

	/**
	 * 
	 * @param nomeFile nome del file da cui attingere (NB: senza estensione)
	 * @throws IOException
	 */
	public Lista (String nomeFile) throws IOException{
		this.nomeFile=nomeFile;

		Scanner scanner = new Scanner(new File(nomeFile+".bst"));
		
		String linea= new String(),el=new String();
		String[] campo=new String[9];
		Cliente cliente;
		
		char l;
		int j, i;
//		int k=0; //TODO debug
		while(scanner.hasNextLine()){
			linea=scanner.nextLine();
			j=0;
			i=0;
			el="";
			while(i<linea.length()){
				l=linea.charAt(i++);
				if (l=='~'){
					campo[j++]=el;
					el="";
				}
				else
					el=el.concat(String.valueOf(l));
			}
			campo[8]=el;
			cliente=new Cliente(campo[0],campo[1],campo[2],campo[3],campo[4],campo[5],campo[6],campo[7],campo[8]);
			listaSchede.add(cliente);
//			System.out.println("%% "+k++ +"  "+i);//debug
		}
		scanner.close();
		System.out.println("lettura file completata;");
		Azione.displayMsg("*LISTA COSTRUTTORE: lettura file completata;");
		PrintWriter pw = new PrintWriter(
				new OutputStreamWriter(
						new FileOutputStream("data/scat.log"),
						"UTF-8"));
		Calendar oggi = Calendar.getInstance();
		SimpleDateFormat formato = new SimpleDateFormat("yyyyMMddhhmma");
		pw.println(formato.format(oggi.getTime())+">>"+"positivo dal mattino!\naltro giorno, altro file.");
		pw.close();
		System.out.println("creazione file di log completata");
		Azione.displayMsg("*LISTA COSTRUTTORE: creazione file di log completata;");
		update=true;
	}

	/**
	 * costruisce una lista dal file di default
	 * @throws IOException
	 */
	public Lista () throws IOException{
		this("data/st");
	}
	
	/**
	 * aggiunge il gliente alla tabella
	 * @param tablerow riga in cui inserire il cliente
	 * @param clienteindex indice del cliente nella listaSchede
	 */
	public void addToTable(int tablerow,int clienteindex){
		Azione.getTable().setValueAt(clienteindex, tablerow, 0);
		Azione.getTable().setValueAt(listaSchede.get(clienteindex).getCognome(), tablerow, 1);
		Azione.getTable().setValueAt(listaSchede.get(clienteindex).getNome(), tablerow, 2);
		Azione.getTable().setValueAt(listaSchede.get(clienteindex).displayColore(), tablerow, 3);
		Azione.getTable().setValueAt(listaSchede.get(clienteindex).displayPermanente(), tablerow, 4);
		Azione.getTable().setValueAt(listaSchede.get(clienteindex).displayTricologico(), tablerow, 5);
		return;
	}
	
	/**
	 * salva la lista attuale sul file aggiungendo la data nel formato yyyyMMddhhmma
	 * @param nomeFileBackup 
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public void backup(String nomeFileBackup) throws UnsupportedEncodingException, FileNotFoundException{
		Calendar oggi = Calendar.getInstance();
		SimpleDateFormat formato = new SimpleDateFormat("yyyyMMddhhmma");
		nomeFileBackup=nomeFileBackup.concat("-"+formato.format(oggi.getTime()));
		PrintWriter pw = new PrintWriter(
				new OutputStreamWriter(
						new FileOutputStream(nomeFileBackup+".bst"),
						"UTF-8"));
		int i=0;
		System.out.println("dimensione lista:" +listaSchede.size());
		Azione.displayMsg("*BACKUP: dimensione lista:" +listaSchede.size()+"\n");
		while(i<listaSchede.size())
			pw.println(listaSchede.get(i++).toFile());
		System.out.println("salvataggio completato in \""+nomeFileBackup+".bst\" ...pare;\nnumero elementi salvati:"+i);
		Azione.displayMsg("*LISTA.BACKUP: salvataggio completato in \""+nomeFileBackup+".bst\" ...pare;\nnumero elementi salvati:"+i);
		pw.close();
		return;
	}
	
	/**
	 * salva la lista attuale sul file data/uabu.bst . 
	 * è un backup automatico che avviene ogni mese 
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public void backup() throws UnsupportedEncodingException, FileNotFoundException{
		Calendar oggi = Calendar.getInstance();
		
		if(oggi.get(Calendar.MONTH)+1 != ultimoAutoBackup()){
			backup("data/abu");
			PrintWriter pw = new PrintWriter(
					new OutputStreamWriter(
							new FileOutputStream("data/uabu.bst"),
							"UTF-8"));
			SimpleDateFormat formato = new SimpleDateFormat("yyyyMMddhhmma");
			pw.println(formato.format(oggi.getTime()));
			pw.close();
		}
		return;
	}
	
	/**
	 * cerca per cognome
	 * @param c cognome
	 * @return indice di posizione nella lista
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public int cercaByC(String c) throws UnsupportedEncodingException, FileNotFoundException{
		c=c.toUpperCase();
		int i=0, primo=-1,j=0;
		String clienti=new String();
		for(i=0; i< listaSchede.size();i++)
			if (listaSchede.get(i).getCognome().equals(c)){
				clienti=clienti.concat("posizione:"+i+"   cognome-nome:"+listaSchede.get(i).getCognome()+" "+listaSchede.get(i).getNome()+"\n");
				if(j<mainWin.NROWS){
					addToTable(j,i);
				}
				j++;
				if (primo==-1)
					primo=i;
			}
		System.out.println(clienti);
		Azione.displayMsg("*LISTA.CERCA_C: clienti trovati:\n"+clienti);
		return  primo;
	}
	
	/**
	 * cerca per cognome e nome
	 * @param c cognome
	 * @param n nome
	 * @return indice di posizione nella lista
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public int cercaByCN(String c,String n) throws UnsupportedEncodingException, FileNotFoundException{
		c=c.toUpperCase();
		n=n.toUpperCase();
		int i=0, primo=-1,j=0;
		String clienti=new String();
		for(i=0; i< listaSchede.size();i++)
			if (listaSchede.get(i).getCognome().equals(c) && listaSchede.get(i).getNome().equals(n)){
				clienti=clienti.concat("posizione:"+i+"   cognome-nome:"+listaSchede.get(i).getCognome()+" "+listaSchede.get(i).getNome()+"\n");
				if(j<mainWin.NROWS){
					addToTable(j,i);
				}
				j++;
				if (primo==-1)
					primo=i;
			}
		System.out.println(clienti);
		Azione.displayMsg("*LISTA.CERCA_CN: clienti trovati\n"+clienti);
		return  primo;
	}
	
	/**
	 * cerca per nome
	 * @param n nome
	 * @return indice di posizione nella lista
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public int cercaByN(String n) throws UnsupportedEncodingException, FileNotFoundException{
		n=n.toUpperCase();
		int i=0, primo=-1,j=0;
		String clienti=new String();
		for(i=0; i< listaSchede.size();i++)
			if (listaSchede.get(i).getNome().equals(n)){
				clienti=clienti.concat("posizione:"+i+"   cognome-nome:"+listaSchede.get(i).getCognome()+" "+listaSchede.get(i).getNome()+"\n");
				if(j<mainWin.NROWS){
					addToTable(j,i);
				}
				j++;
				if (primo==-1)
					primo=i;
			}
		System.out.println(clienti);
		Azione.displayMsg("*CERCA N: clienti trovati:\n"+clienti+"\n");
		return  primo;
	}
	
	/**
	 * cerca i nomi e i cognomi contenenti la stringa t
	 * @param t stringa da ricercare nel nome e/o nel cognome
	 * @return indice di posizione nella lista
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public int cercaSimultanea(String t) throws UnsupportedEncodingException, FileNotFoundException{
		t=t.toUpperCase();
		int i=0, primo=-1,j=0;
		String clienti=new String();
		for(i=0; i< listaSchede.size();i++)
			if (listaSchede.get(i).getCognome().contains(t) || listaSchede.get(i).getNome().contains(t)){
				clienti=clienti.concat("posizione:"+i+"   cognome-nome:"+listaSchede.get(i).getCognome()+" "+listaSchede.get(i).getNome()+"\n");
				if(j<mainWin.NROWS){
					addToTable(j,i);
				}
				j++;
				if (primo==-1)
					primo=i;
			}
		System.out.println(clienti);
		Azione.displayMsg("*LISTA.CERCA_CN: clienti trovati\n"+clienti);
		return  primo;
	}
	
	/**
	 * restituisce il cliente alla posizione richiesta
	 * @param i indice di posizione del cliente nella lista
	 * @return cliente
	 */
	public Cliente getCliente(int i){
		if (i>=listaSchede.size())
			return null;
		return listaSchede.get(i);
	}
	
	/**
	 * 
	 * @return false se ci sono modifica da salvare su file; true altrimenti 
	 */
	public boolean isUpdate(){
		return update;
	}
	
	/**
	 * inserimento nuovo cliente
	 * @param c cognome
	 * @param n nome
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 * @throws HeadlessException 
	 */
	public void newCliente(String c, String n) throws HeadlessException, UnsupportedEncodingException, FileNotFoundException{
		if(this.cercaByCN(c,n)!=-1){
			System.out.println("cliente gia presente;");
			Azione.displayMsg("*LISTA.NEW: cliente gia presente;");
			int opt = JOptionPane.showConfirmDialog(
					mainWin.frame,
				    "Esiste già un cliente di nome:\n"+mainWin.txtDc.getText().toUpperCase()+" "+mainWin.txtDn.getText().toUpperCase()+
				    "\n\nannullare l'operazione ?",
				    "Cliente già presente",
				    JOptionPane.YES_NO_OPTION);
			if(opt==0){
				return;
			}
		}
		Cliente cliente= new Cliente(c,n);
		listaSchede.add(cliente);
		ordina();
		update=false;
		return;
	}
	/**
	 * inserimento nuovo cliente
	 * @param cliente
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 * @throws HeadlessException 
	 */
	public void newCliente(Cliente cliente) throws HeadlessException, UnsupportedEncodingException, FileNotFoundException{
		if(this.cercaByCN(cliente.getCognome(),cliente.getNome())!=-1){
			System.out.println("cliente gia presente;");
			Azione.displayMsg("*LISTA.NEW: cliente gia presente;");
			int opt = JOptionPane.showConfirmDialog(
					mainWin.frame,
				    "Esiste già un cliente di nome:\n"+mainWin.txtDc.getText().toUpperCase()+" "+mainWin.txtDn.getText().toUpperCase()+
				    "\n\nannullare l'operazione ?",
				    "Cliente già presente",
				    JOptionPane.YES_NO_OPTION);
			if(opt==0){
				mainWin.schede.removeCliente(mainWin.txtDc.getText(),mainWin.txtDn.getText());
				return;
			}
		}
		listaSchede.add(cliente);
		ordina();
		update=false;
		return;
	}
	
	//TODO da controllare se va
	// e se è applicata nei punti giusti
	/**
	 * riordina listaSchede in ordine alfabetico
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public void ordina() throws UnsupportedEncodingException, FileNotFoundException{
		Cliente tmp=new Cliente();
		int i,j;
		
		for(i=1;i<listaSchede.size();i++){
			tmp=listaSchede.get(i);
			
			j=i-1;
			while(j>=0 && listaSchede.get(j).maggiore(tmp)){
				listaSchede.set(j+1, listaSchede.get(j));
				j--;
			}
			System.out.println("j "+j+"  i "+i);	
			listaSchede.set(j+1, tmp);
		}
		update=false;
		return;
	}

	/**
	 * cancella un cliente 
	 * @param c cognome
	 * @param n nome
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public void removeCliente(String c, String n) throws UnsupportedEncodingException, FileNotFoundException{
		int i=this.cercaByCN(c,n);
		if(i!=-1){
			String cliente=listaSchede.get(i).getCognome()+" "+listaSchede.get(i).getNome();
			listaSchede.remove(i);
			Azione.displayMsg("*LISTA.REMOVE: "+cliente+" eliminato;");
			update=false;
		}
		else{
			System.out.println("cliente non trovato;");
			Azione.displayMsg("*LISTA.REMOVE: cliente non trovato;");
		}
		return;
	}
	
	/**
	 * rimuovi cliente
	 * @param i indice di posizione
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public void removeCliente(int i) throws UnsupportedEncodingException, FileNotFoundException{
		if(i!=-1){
			String cliente=listaSchede.get(i).getCognome()+" "+listaSchede.get(i).getNome();
			listaSchede.remove(i);
			Azione.displayMsg("*LISTA.REMOVE: "+cliente+" eliminato;");
			update=false;
		}
		else{
			System.out.println("cliente non trovato;");
			Azione.displayMsg("*LISTA.REMOVE: cliente non trovato;");
		}
		return;
	}
	
	/**
	 * salva la lista attuale sul file specificato
	 * @param nomeFileSalvataggio
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public void salva(String nomeFileSalvataggio) throws UnsupportedEncodingException, FileNotFoundException{
		PrintWriter pw = new PrintWriter(
				new OutputStreamWriter(
						new FileOutputStream(nomeFileSalvataggio+".bst"),
						"UTF-8"));
		int i=0;
		System.out.println("dimensione lista:" +listaSchede.size());
		Azione.displayMsg("*LISTA.SALVA: dimensione lista:" +listaSchede.size());
		while(i<listaSchede.size())
			pw.println(listaSchede.get(i++).toFile());
		System.out.println("salvataggio completato in \""+nomeFileSalvataggio+".bst\" ...pare;\nnumero elementi salvati:"+i);
		Azione.displayMsg("*LISTA.SALVA: salvataggio completato in \""+nomeFileSalvataggio+".bst\" ...pare;\nnumero elementi salvati:"+i);
		pw.close();
		return;
	}
	
	/**
	 * salva la lista attuale sul file usato per la lettura
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public void salva() throws UnsupportedEncodingException, FileNotFoundException{
		this.salva(nomeFile);
		return;
	}
	public void salvataggioAutomatico() throws FileNotFoundException, UnsupportedEncodingException{
		Calendar oggi = Calendar.getInstance();
		if(oggi.get(Calendar.HOUR)+1 != ultimoSalvataggioAutomatico()){
			salva("data/sa"); // salvataggio automatico schede tecniche
			PrintWriter pw = new PrintWriter(
					new OutputStreamWriter(
							new FileOutputStream("data/usa.bst"), // ultimo salvataggio automatico schede tecniche
							"UTF-8"));
			SimpleDateFormat formato = new SimpleDateFormat("yyyyMMddhhmma");
			pw.println(formato.format(oggi.getTime()));
			pw.close();
		}
		return;
	}
	/**
	 * 
	 * @return lunghezza listaSchede
	 */
	public int size(){
		return listaSchede.size();
	}
	
	/**
	 * stabilisce il mese dell'ultimo backup
	 * appoggia la funzione backup()
	 * @return 1: gen; 2: feb 3: mar; ...
	 * @throws FileNotFoundException
	 */
	private int ultimoAutoBackup() throws FileNotFoundException{
		Scanner scanner = new Scanner(new File("data/uabu.bst"));
		int d=Integer.parseInt(scanner.nextLine().substring(4,6));
		scanner.close();
		return d;
	}
	private int ultimoSalvataggioAutomatico() throws FileNotFoundException{
		Scanner scanner = new Scanner(new File("data/usa.bst"));
		int d=Integer.parseInt(scanner.nextLine().substring(8,10));
		scanner.close();
		return d;
	}
	
	/**
	 * comunica che ci sono dati da salvare su file
	 */
	public static void updateFalse(){
		update=false;
		return;
	}
}