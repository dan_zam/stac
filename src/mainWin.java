/*************************************************************************
 *  
 * Author: 
 *    Daniele Zambon
 * 
 * License: 
 *    Copyright (c) 2013-2019 and later, Daniele Zambon.
 *    Copyright (c) 2002 and later by MH Software-Entwicklung. 
 *    All Rights Reserved.
 *
 *  This file is part of Stac.
 *
 *  Stac is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Stac is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Stac.  If not, see <http://www.gnu.org/licenses/>.
 *
 ************************************************************************/

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.jtattoo.plaf.texture.TextureLookAndFeel;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import java.io.File;

public class mainWin {

    public static String VERSION = "1.1";
	
    public static mainWin window;
	
	public static Lista schede;
	
	public static JFrame frame;
	
	public static JTextField txtNome;
	public static JTextField txtCognome;
	public static JTextField txtDn;
	public static JTextField txtDc;
	public static JTextArea txtrDcol;
	public static JTextArea txtrDper;
	public static JTextArea txtrDtri;
	public static JTextField txtTel;
	public static JTextField txtVia;
	public static JTextField txtCap;
	public static JTextField txtPaese;
	public static JTextField txtSfoglia;
	public static JTextField txtId;
 	
	public JButton btnInserisci;
	public JButton btnModifica;
	public static JButton btnConfermaInserimento;
	public static JButton btnConfermaModifiche;
	public static JButton btnInfo;
	public JButton btnCerca;
	public static JButton btnAnnulla;
	public JButton btnElimina;
	public JButton btnCercaNome;
	public JButton btnCercaCognome;
	
	private JScrollPane panel_e;
	private JPanel panel_n;

	public static JTable table;
 	private static String[][] data;
	
	private JMenuBar menuBar;
	private JMenu mnFile;
	private JMenuItem mntmSalva;
	private JMenuItem mntmSalvaConNome;
	private JMenuItem mntmEsci;
	private JMenuItem mntmApri;

 	public static int NROWS;
	public static JTextArea txtrOutput=new JTextArea();
	private JMenu mnAiuto;
	private JMenuItem mntmIstruzioni;
	private JMenuItem mntmInformazioni;
	private JMenu mnModifica;
	private JMenu mnPreferenze;
	private JMenuItem mntmSalvataggioChiavetta;
	
	private static Image icon;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
        for(int i=0; i<args.length;i++){
            if(args[i].equals("-v") || args[i].equals("--version")){
                System.out.println(mainWin.VERSION);
                return;
            }
        }
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					icon = Toolkit.getDefaultToolkit().getImage("images/stac.png");

		            Properties props = new Properties();
		            props.put("logoString", "stac"); 
		            props.put("backgroundColorLight","250 0 0");
		            props.put("backgroundColorDark","150 50 50");
		            props.put("selectionBackgroundColor", "230 50 50"); 
		            props.put("selectionForegroundColor", "254 254 254");
		            props.put("menuSelectionBackgroundColor", "180 240 197"); 
		            props.put("controlColor", "218 254 230");
		            props.put("controlColorLight", "218 254 230");
		            props.put("controlColorDark", "180 240 197"); 
		            props.put("buttonColor", "218 230 254");
		            props.put("buttonColorLight", "255 255 255");
		            props.put("buttonColorDark", "244 242 232");
		            props.put("rolloverColor", "254 50 50"); 
		            props.put("rolloverColorLight", "154 50 50"); 
		            props.put("rolloverColorDark", "240 0 0"); 
		            props.put("windowTitleForegroundColor", "254 254 254");
		            props.put("windowTitleBackgroundColor", "180 240 197"); 
		            props.put("windowTitleColorLight", "218 254 230"); 
		            props.put("windowTitleColorDark", "180 240 197"); 
		            props.put("windowBorderColor", "218 254 230");
		            props.put("textureSet","Rock");

		            TextureLookAndFeel.setCurrentTheme(props);
		            UIManager.setLookAndFeel("com.jtattoo.plaf.texture.TextureLookAndFeel");

                    // check presence of installation files and conformity
                    List<String> required_files = new ArrayList<>();
                    required_files.add("data/st.bst");
                    required_files.add("data/salvataggio-chiavetta.bst");
                    List<String> required_files_last_update = new ArrayList<>();
                    required_files_last_update.add("data/uabu.bst");
                    required_files_last_update.add("data/usa.bst");
                    required_files.addAll(required_files_last_update);
                    for(String rf : required_files){
                        if (new File(rf).exists()){}
                        else{throw new FileNotFoundException("file ".concat(rf).concat(" was not found...\nDid you go through the installation process?"));}
                    }
                    for(String rf : required_files_last_update){
                        Scanner scanner = new Scanner(new File(rf));
                        String line;
                        if (scanner.hasNextLine()){} 
                        else{throw new Exception("It looks like that ".concat(rf).concat(" is not well formatted"));}
                        line = scanner.nextLine();
                        if (line.length() == 14 && line.substring(13).equals("M")){} 
                        else{throw new Exception("It looks like that ".concat(rf).concat(" is not well formatted"));}
                    }

                    // set up program
		            schede = new Lista("data/st");
					window = new mainWin();
					mainWin.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
                    JOptionPane.showMessageDialog(frame,
                            e,
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public mainWin() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		UIManager.put("Button.defaultButtonFollowsFocus", Boolean.TRUE);
		
		frame = new JFrame("Artemoda Claudio - Schede Tecniche");
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					Azione.btnChiudiAction();
				} catch (UnsupportedEncodingException | FileNotFoundException e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(frame,
						    e1,
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		frame.setBounds(50, 50, 1200, 750);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));		
        frame.setIconImage(icon);

		// WEST ---------------------------------------------------------------------------------------------------------------------------------

		JPanel panel_w = new JPanel();
		frame.getContentPane().add(panel_w, BorderLayout.WEST);
		
		GroupLayout gl_panel_w = new GroupLayout(panel_w);
		gl_panel_w.setHorizontalGroup(
			gl_panel_w.createParallelGroup(Alignment.LEADING)
				.addGap(0, 10, Short.MAX_VALUE)
		);
		gl_panel_w.setVerticalGroup(
			gl_panel_w.createParallelGroup(Alignment.LEADING)
				.addGap(0, 555, Short.MAX_VALUE)
		);
		panel_w.setLayout(gl_panel_w);

		// CENTER ---------------------------------------------------------------------------------------------------------------------------------
		
		JPanel panel_c = new JPanel();
		frame.getContentPane().add(panel_c, BorderLayout.CENTER);
		
		txtDn = new JTextField();
		txtDn.setColumns(10);
		txtDn.setEditable(false);
		txtDn.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				txtDn.selectAll();
			}
		});
		
		txtDc = new JTextField();
		txtDc.setColumns(10);
		txtDc.setEditable(false);
		txtDc.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				txtDc.selectAll();
			}
		});
		
		txtrDcol = new JTextArea(3,60);
		txtrDcol.setEditable(false);
		
		txtrDper = new JTextArea(3,60);
		txtrDper.setEditable(false);
		
		txtrDtri = new JTextArea(3,60);
		txtrDtri.setEditable(false);
		
		btnConfermaInserimento = new JButton("conferma inserimento");
		btnConfermaInserimento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Azione.btnConfermaInserimentoAction();
				} catch (HeadlessException | UnsupportedEncodingException
						| FileNotFoundException e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(frame,
						    e1,
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnConfermaInserimento.setVisible(false);
		
		txtTel = new JTextField();
		txtTel.setColumns(10);
		txtTel.setEditable(false);
		txtTel.setVisible(false);
		
		txtVia = new JTextField();
		txtVia.setColumns(10);
		txtVia.setEditable(false);
		txtVia.setVisible(false);
		
		txtCap = new JTextField();
		txtCap.setColumns(10);
		txtCap.setEditable(false);
		txtCap.setVisible(false);
		
		txtPaese = new JTextField();
		txtPaese.setColumns(10);
		txtPaese.setEditable(false);
		txtPaese.setVisible(false);
		
		btnInfo = new JButton("info");
		btnInfo.setVisible(false);
		btnInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Azione.btnInfoAction();
				txtrDcol.setText(txtrDcol.getText());
			}
		});
		
		JLabel lblNome = new JLabel("Nome");
		JLabel lblCognome = new JLabel("Cognome");
		JLabel lblColore = new JLabel("colore");
		JLabel lblPermanente = new JLabel("permanente");
		JLabel lblTricologico = new JLabel("tricologico");
		
		btnConfermaModifiche = new JButton("conferma modifiche");
		btnConfermaModifiche.setVisible(false);
		btnConfermaModifiche.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Azione.btnConfermaModificheAction();
				} catch (UnsupportedEncodingException | FileNotFoundException e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(frame,
						    e1,
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		btnAnnulla = new JButton("annulla");
		btnAnnulla.setVisible(false);
		btnAnnulla.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Azione.btnAnnullaAction();
			}
		});
		
		txtId = new JTextField();
		txtId.setText("id");
		txtId.setColumns(10);
		txtId.setVisible(false);
		
		GroupLayout gl_panel_c = new GroupLayout(panel_c);
		gl_panel_c.setHorizontalGroup(
			gl_panel_c.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_c.createSequentialGroup()
					.addGap(5)
					.addGroup(gl_panel_c.createParallelGroup(Alignment.LEADING)
						.addComponent(txtDc, GroupLayout.PREFERRED_SIZE, 178, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblColore)
						.addComponent(txtrDcol, GroupLayout.PREFERRED_SIZE, 440, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblPermanente)
						.addComponent(txtrDper, GroupLayout.PREFERRED_SIZE, 440, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblTricologico)
						.addComponent(txtrDtri, GroupLayout.PREFERRED_SIZE, 440, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panel_c.createSequentialGroup()
							.addGroup(gl_panel_c.createParallelGroup(Alignment.LEADING)
								.addComponent(btnInfo)
								.addComponent(txtTel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtVia, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtCap, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtPaese, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(150)
							.addGroup(gl_panel_c.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_c.createParallelGroup(Alignment.TRAILING)
									.addComponent(btnAnnulla)
									.addComponent(btnConfermaModifiche)
									.addComponent(btnConfermaInserimento))))
						.addGroup(gl_panel_c.createSequentialGroup()
							.addGroup(gl_panel_c.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_panel_c.createSequentialGroup()
									.addGroup(gl_panel_c.createParallelGroup(Alignment.TRAILING)
										.addGroup(gl_panel_c.createSequentialGroup()
											.addComponent(lblCognome)
											.addGap(130)))
									.addGroup(gl_panel_c.createParallelGroup(Alignment.LEADING)
										.addComponent(lblNome)
										.addComponent(txtDn, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE))))
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(txtId, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)))
					.addContainerGap(19, GroupLayout.PREFERRED_SIZE))
		);
		gl_panel_c.setVerticalGroup(
			gl_panel_c.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_c.createSequentialGroup()
					.addGroup(gl_panel_c.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_c.createSequentialGroup()
							.addGap(10)
							.addGroup(gl_panel_c.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblCognome)
								.addComponent(lblNome))
							.addGap(5)
							.addGroup(gl_panel_c.createParallelGroup(Alignment.LEADING)
								.addComponent(txtDn, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtDc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtId, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(10)
							.addComponent(lblColore))
						.addGroup(gl_panel_c.createSequentialGroup()))
					.addGap(5)
					.addComponent(txtrDcol, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(lblPermanente)
					.addGap(5)
					.addComponent(txtrDper, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(lblTricologico)
					.addGap(5)
					.addComponent(txtrDtri, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addGroup(gl_panel_c.createParallelGroup(Alignment.TRAILING, false)
						.addGroup(gl_panel_c.createSequentialGroup()
							.addComponent(btnInfo)
							.addGap(10)
							.addComponent(txtTel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(5)
							.addComponent(txtVia, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(5)
							.addComponent(txtCap, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(5)
							.addComponent(txtPaese, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel_c.createSequentialGroup()
							.addGap(8)
							.addComponent(btnAnnulla)
							.addGap(5)
							.addComponent(btnConfermaInserimento)
							.addGap(5)
							.addComponent(btnConfermaModifiche)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel_c.setLayout(gl_panel_c);

		// EAST ---------------------------------------------------------------------------------------------------------------------------------
		
		NROWS=schede.size()+5;
		data= new String[NROWS][6];
		for (int j=0;j<NROWS;j++)
			data[j][0]="-1";
		for (int i=1;i<6;i++)
			for (int j=0;j<NROWS;j++)
				data[j][i]="";
		String[] columnNames = {"nr","cognome", "nome","colore","permanente","tricologico"};

		table = new JTable(new DefaultTableModel(data,columnNames));
	 	
	 	table.getColumnModel().getColumn(0).setPreferredWidth(40);
	 	table.getColumnModel().getColumn(1).setPreferredWidth(80);
	 	table.getColumnModel().getColumn(2).setPreferredWidth(80);
	 	table.getColumnModel().getColumn(3).setPreferredWidth(170);
	 	table.getColumnModel().getColumn(4).setPreferredWidth(170);
	 	table.getColumnModel().getColumn(5).setPreferredWidth(170);

	 	panel_e = new JScrollPane(table);
	 	panel_e.setPreferredSize(new Dimension(710,200));
		frame.getContentPane().add(panel_e, BorderLayout.EAST);
		
		table.addMouseListener(new java.awt.event.MouseAdapter(){
			public void mouseClicked(java.awt.event.MouseEvent e){
				Azione.tablesRowAction(e);
			}
		});
		
		// NORTH ---------------------------------------------------------------------------------------------------------------------------------
		
		panel_n = new JPanel();
		frame.getContentPane().add(panel_n, BorderLayout.NORTH);
		
		txtNome = new JTextField();
		txtNome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNome.transferFocus();
			}
		});
		txtNome.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				txtNome.selectAll();
			}
		});
		txtNome.setText("nome...");
		txtNome.setColumns(10);
		
		txtCognome = new JTextField();
		txtCognome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtCognome.transferFocus();
			}
		});
		txtCognome.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				txtCognome.selectAll();
			}
		});
		txtCognome.setText("cognome...");
		txtCognome.setColumns(10);
		
		btnCerca = new JButton("cerca");
		btnCerca.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Azione.btnCercaAction(2);
				} catch (UnsupportedEncodingException | FileNotFoundException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(frame,
						    e,
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		btnCercaNome = new JButton("cerca per nome");
		btnCercaNome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Azione.btnCercaAction(0);
				} catch (UnsupportedEncodingException | FileNotFoundException e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(frame,
						    e1,
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
				}				
			}
		});
		
		btnCercaCognome = new JButton("cerca per cognome");
		btnCercaCognome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Azione.btnCercaAction(1);
				} catch (UnsupportedEncodingException | FileNotFoundException e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(frame,
						    e1,
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		txtrOutput.setBackground(new Color(169, 169, 169));
		
		txtrOutput.setText("");
		txtrOutput.setEditable(false);
		GroupLayout gl_panel_n = new GroupLayout(panel_n);
		gl_panel_n.setHorizontalGroup(
			gl_panel_n.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_n.createSequentialGroup()
					.addContainerGap()
					.addComponent(txtCognome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtNome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnCerca, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_n.createParallelGroup(Alignment.LEADING, false)
						.addComponent(btnCercaNome, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnCercaCognome, GroupLayout.PREFERRED_SIZE, 186, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
					.addComponent(txtrOutput, GroupLayout.PREFERRED_SIZE, 612, GroupLayout.PREFERRED_SIZE))
		);
		gl_panel_n.setVerticalGroup(
			gl_panel_n.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_n.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_n.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_n.createParallelGroup(Alignment.BASELINE)
							.addComponent(txtCognome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(txtNome, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_n.createSequentialGroup()
							.addComponent(btnCercaNome)
							.addGap(0)
							.addComponent(btnCercaCognome))
						.addComponent(btnCerca, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addComponent(txtrOutput, GroupLayout.PREFERRED_SIZE, 66, Short.MAX_VALUE)
		);
		panel_n.setLayout(gl_panel_n);
		panel_n.setBorder(BorderFactory.createMatteBorder(0,0,2,0, Color.gray));

		
		// SOUTH ---------------------------------------------------------------------------------------------------------------------------------
		
		JPanel panel_s = new JPanel();
		frame.getContentPane().add(panel_s, BorderLayout.SOUTH);
		
		btnElimina = new JButton("elimina");
		btnElimina.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Azione.btnEliminaAction();
				} catch (UnsupportedEncodingException | FileNotFoundException e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(frame,
						    e1,
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		btnModifica = new JButton("modifica");
		btnModifica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Azione.btnModificaAction();
			}
		});
		
		btnInserisci = new JButton("inserisci nuovo cliente");
		btnInserisci.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				Azione.btnInserisciAction();
			}
		});
		
		JButton btnSfoglia = new JButton("sfoglia");
		btnSfoglia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Azione.btnSfogliaAction();
			}
		});
		
		txtSfoglia = new JTextField();
		txtSfoglia.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				try {
					Azione.txtSfogliaAction();
				} catch (UnsupportedEncodingException | FileNotFoundException e1) {
					e1.printStackTrace();
					JOptionPane.showMessageDialog(frame,
						    e1,
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		txtSfoglia.setColumns(10);
		
		JButton btnTest = new JButton("test");
		btnTest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Azione.btnTestAction();
				} catch (UnsupportedEncodingException | FileNotFoundException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(frame,
						    e,
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		GroupLayout gl_panel_s = new GroupLayout(panel_s);
		gl_panel_s.setHorizontalGroup(
			gl_panel_s.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_s.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnModifica)
					.addGap(72)
					.addComponent(btnElimina)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnInserisci)
					.addGap(61)
					.addComponent(btnTest)
					.addPreferredGap(ComponentPlacement.RELATED, 325, Short.MAX_VALUE)
					.addComponent(btnSfoglia)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtSfoglia, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(112))
		);
		gl_panel_s.setVerticalGroup(
			gl_panel_s.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_s.createSequentialGroup()
					.addGroup(gl_panel_s.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_s.createSequentialGroup()
							.addGroup(gl_panel_s.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnElimina)
								.addComponent(btnInserisci)
								.addComponent(btnModifica)
								.addComponent(btnTest)
								.addComponent(txtSfoglia, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnSfoglia)
						.addGroup(gl_panel_s.createParallelGroup(Alignment.BASELINE))))))
		);
		panel_s.setLayout(gl_panel_s);
		panel_s.setBorder(BorderFactory.createMatteBorder(2,0,0,0, Color.gray));

		// MENU ---------------------------------------------------------------------------------------------------------------------------------
		
		menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		mntmSalva = new JMenuItem("salva");
		mntmSalva.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Azione.salvaAction(0);
			}
		});
		mnFile.add(mntmSalva);
		
		mntmSalvaConNome = new JMenuItem("salva con nome");
		mntmSalvaConNome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Azione.salvaAction(1);
			}
		});
		mnFile.add(mntmSalvaConNome);
		
		mntmApri = new JMenuItem("apri");
		mntmApri.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Azione.btnApriAction();
				} catch (IOException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(frame,
						    e,
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		mnFile.addSeparator();
		mnFile.add(mntmApri);
		
		mnFile.addSeparator();
		mntmEsci = new JMenuItem("Esci");
		mnFile.add(mntmEsci);
		
		mnModifica = new JMenu("Modifica");
		menuBar.add(mnModifica);
		
		mnPreferenze = new JMenu("Preferenze");
		mnModifica.add(mnPreferenze);
		
		mntmSalvataggioChiavetta = new JMenuItem("salvataggio chiavetta");
		mntmSalvataggioChiavetta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Azione.mnSalvataggioChiavettaAction();
				} catch (FileNotFoundException | UnsupportedEncodingException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(frame,
						    e,
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnPreferenze.add(mntmSalvataggioChiavetta);
		
		mnAiuto = new JMenu("Aiuto");
		menuBar.add(mnAiuto);
		
		mntmIstruzioni = new JMenuItem("istruzioni");
		mntmIstruzioni.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Azione.mnIstruzioniAction();
			}
		});
		mnAiuto.add(mntmIstruzioni);
		
		mntmInformazioni = new JMenuItem("informazioni");
		mntmInformazioni.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Azione.mnInformazioniAction();
			}
		});
		mnAiuto.add(mntmInformazioni);
	}
}
