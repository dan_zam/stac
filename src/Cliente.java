/*************************************************************************
 *  
 * Author: 
 *    Daniele Zambon
 * 
 * License: 
 *    Copyright (c) 2013-2019 and later, Daniele Zambon.
 *    Copyright (c) 2002 and later by MH Software-Entwicklung. 
 *    All Rights Reserved.
 *
 *  This file is part of Stac.
 *
 *  Stac is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Stac is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Stac.  If not, see <http://www.gnu.org/licenses/>.
 *
 ************************************************************************/

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import javax.swing.JOptionPane;


public class Cliente {
	private String  cognome, nome, colore, permanente, tricologico, telefono,via ,cap ,paese ;
	
	/**
	 * 
	 * @param c cognome
	 * @param n nome
	 * @param col specifiche colore
	 * @param per specifiche permanente
	 * @param tri specifiche tricologico
	 * @param t	numero di telefono
	 * @param v via
	 * @param cap codice di avviamento postale
	 * @param p paese
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public Cliente(String c,String n, String col,String per,String tri,String t,String v,String cap,String p) throws UnsupportedEncodingException, FileNotFoundException{
		cognome=c.toUpperCase();
		nome=n.toUpperCase();
		setTrattamenti(col,per,tri);
		telefono=t;
		via=v;
		this.cap=cap;
		paese=p;
	}
	
	/**
	 * 
	 * @param c cognome
	 * @param n nome
	 * @param col specifiche colore
	 * @param per specifiche permanente
	 * @param tri specifiche tricologico
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public Cliente(String c,String n, String col,String per,String tri) throws UnsupportedEncodingException, FileNotFoundException{
		this(c,n,col,per,tri,"","","","");	
	}
	
	/**
	 * 
	 * @param c cognome
	 * @param n nome
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public Cliente(String c, String n) throws UnsupportedEncodingException, FileNotFoundException{
		this(c,n,"","","","","","","");
	}
	
	/**
	 * cliente nullo
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public Cliente() throws UnsupportedEncodingException, FileNotFoundException{
		this("","","","","","","","","");
	}
	/**
	 * 
	 * @param cols numero massimo di colonne
	 * @return il campo colore in formato di lettura su righe di al massimo cols colonne
	 */
	public String displayColore(int cols,int rows){
		String txt=colore;
		int c=0,r=1;
		for(int j=0;j<txt.length();j++){
			c++;
			if (txt.charAt(j)=='\n'){
				c=0;
				r++;
			}
			if(c==cols){
				txt=txt.substring(0,j+1)+"\n"+txt.substring(j+1);
				c=0;
				j++;
				r++;
			}
		}
		if (r>rows)
	        JOptionPane.showMessageDialog(mainWin.frame,
				    "Attenzione, del testo del campo COLORE potrebbe\n"+
				    "essere oltre lo spazio visibile.",
				    "Attenzione",
				    JOptionPane.INFORMATION_MESSAGE);
		return txt;
	}
	/**
	 * 
	 * @return il campo colore su una singola riga
	 */
	public String displayColore(){
		return colore;
	}
	/**
	 * 
	 * @return i campi telefono, via, cap e paese in formato di lettura
	 */
	public String displayInfo(){
		return "tel: "+telefono+"\n---\n"+via+cap+paese;
	}
	/**
	 * 
	 * @param cols numero massimo di colonne
	 * @return il campo permanente in formato di lettura su righe di al massimo cols colonne
	 */
	public String displayPermanente(int cols,int rows){
		String txt=permanente;
		int c=0,r=1;
		for(int j=0;j<txt.length();j++){
			c++;
			if (txt.charAt(j)=='\n')
				c=0;
			if(c==cols){
				txt=txt.substring(0,j+1)+"\n"+txt.substring(j+1);
				c=0;
				r++;
			}
		}
		if (r>rows)
	        JOptionPane.showMessageDialog(mainWin.frame,
				    "Attenzione, del testo del campo PERMANENTE potrebbe\n"+
				    "essere oltre lo spazio visibile.",
				    "Attenzione",
				    JOptionPane.INFORMATION_MESSAGE);
		return txt;
	}
	/**
	 * 
	 * @return il campo permanente su una singola riga
	 */
	public String displayPermanente(){
		return permanente;
	}

	/**
	 * 
	 * @param cols numero massimo di colonne
	 * @return il campo tricologico in formato di lettura su righe di al massimo cols colonne
	 */
	public String displayTricologico(int cols,int rows){
		String txt=tricologico;
		int c=0,r=1;
		for(int j=0;j<txt.length();j++){
			c++;
			if (txt.charAt(j)=='\n')
				c=0;
			if(c==cols){
				txt=txt.substring(0,j+1)+"\n"+txt.substring(j+1);
				c=0;
				r++;
			}
		}
		if (r>rows)
	        JOptionPane.showMessageDialog(mainWin.frame,
				    "Attenzione, del testo del campo TRICOLOGICO potrebbe\n"+
				    "essere oltre lo spazio visibile.",
				    "Attenzione",
				    JOptionPane.INFORMATION_MESSAGE);
		return txt;
	}
	/**
	 * 
	 * @return il campo tricologico su una singola riga
	 */
	public String displayTricologico(){
		return tricologico;
	}
	/**
	 * 
	 * @return cap
	 */
	public String getCap(){
		return cap;
	}
	/**
	 * 
	 * @return cognome
	 */
	public String getCognome(){
		return cognome;
	}
	/**
	 * 
	 * @return nome
	 */
	public String getNome(){
		return nome;
	}
	/**
	 * 
	 * @return paese
	 */
	public String getPaese(){
		return paese;
	}
	/**
	 * 
	 * @return telefono
	 */
	public String getTelefono(){
		return telefono;
	}
	/**
	 * 
	 * @return via
	 */
	public String getVia(){
		return via;
	}
	/**
	 * stabilisce la relazione d'ordine totale attraverso l'alfabeto (ordine alfabetico)
	 * @param c secondo cliente
	 * @return true se this segue c in ordine alfebetico false se lo precede
	 */
	public boolean maggiore(Cliente c){
		if (this.getCognome().compareTo(c.getCognome())==0)
			return this.getNome().compareTo(c.getNome())>0;
		return this.getCognome().compareTo(c.getCognome())>0;	
	}
	/**
	 * imposta o modifica le specifiche colore
	 * @param col specifiche permanente
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public void setColore(String col) throws UnsupportedEncodingException, FileNotFoundException{
		for(int i=0;i<col.length();i++)
			if(col.charAt(i)=='\n')
				col=col.substring(0,i)+" "+col.substring(i+1);
		colore=col;
		Azione.displayMsg("*CLIENTE.SET_COLORE: fatto;");
		Lista.updateFalse();
		return;
	}
	/**
	 * imposta o modifica il cognome
	 * @param c cognome
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public void setCognome(String c) throws UnsupportedEncodingException, FileNotFoundException{
		cognome=c.toUpperCase();
		Azione.displayMsg("*CLIENTE.SET_C: fatto;");
		Lista.updateFalse();
		return;
	}
	
	/**
	 * imposta o modifica il nome
	 * @param n nome
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public void setNome(String n) throws UnsupportedEncodingException, FileNotFoundException{
		nome=n.toUpperCase();
		Azione.displayMsg("*CLIENTE.SET_N: fatto;");
		Lista.updateFalse();
		return;
	}
	/**
	 * imposta o modifica le informazioni telefono via cap e paese
	 * @param tel telefono
	 * @param v via
	 * @param c cap
	 * @param p paese
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public void setInfo(String tel,String v,String c,String p) throws UnsupportedEncodingException, FileNotFoundException{
		telefono=tel;
		via=v;
		cap=c;
		paese=p;
		Azione.displayMsg("*CLIENTE.SET_INFO: fatto;");
		Lista.updateFalse();
		return;
	}
	/**
	 * imposta o modifica le specifiche permanente
	 * @param per specifiche permanente
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public void setPermanente(String per) throws UnsupportedEncodingException, FileNotFoundException{
		for(int i=0;i<per.length();i++)
			if(per.charAt(i)=='\n')
				per=per.substring(0,i)+" "+per.substring(i+1);
		permanente=per;
		Azione.displayMsg("*CLIENTE.SET_PERMANENTE: fatto;");
		Lista.updateFalse();
		return;
	}
	/**
	 * imposta o modifica il numerod i telefono 
	 * @param tel numero di telefono
	 */
	public void setTelefono(String tel){
		telefono=tel;
		Lista.updateFalse();
		return;
	}
	/**
	 * imposta o modifica i trattamenti di colore, permanente e tricologico
	 * @param col spacifiche colore
	 * @param per spacifiche permanente
	 * @param tri spacifiche tricologico
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public void setTrattamenti(String col,String per,String tri) throws UnsupportedEncodingException, FileNotFoundException{
		setColore(col);
		setPermanente(per);
		setTricologico(tri);
		Azione.displayMsg("*CLIENTE.SET_TRATTAMENTI: fatto;");
		Lista.updateFalse();
		return;
	}
	/**
	 * imposta o modifica le specifiche tricologico
	 * @param tri specifiche tricologico
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	public void setTricologico(String tri) throws UnsupportedEncodingException, FileNotFoundException{
		for(int i=0;i<tri.length();i++)
			if(tri.charAt(i)=='\n')
				tri=tri.substring(0,i)+" "+tri.substring(i+1);
		Azione.displayMsg("*CLIENTE.SET_TRICOLOGICO: fatto;");
		tricologico=tri;
		Lista.updateFalse();
		return;
	}
	/**
	 * 
	 * @return setta i campi nel formato per finestre di dialogo
	 */
	public String toDialog(){
		return "--- --- ---\n * cognome: "+cognome+"\n * nome:    "+nome+"\n---\n * colore:\n"+colore+"\n * permanente:\n"+permanente+"\n * tricologico:\n"+tricologico+"\n---\n * tel:  "+telefono+"\n * via:  "+via+"\n * cap:  "+cap+"\n * paese:"+paese+"\n--- --- ---";
	}
	/**
	 * 
	 * @return setta i campi nel formato per il salvataggio su file
	 */
	public String toFile(){
		return cognome+"~"+nome+"~"+colore+"~"+permanente+"~"+tricologico+"~"+telefono+"~"+via+"~"+cap+"~"+paese;
	}
	/**
	 * setta i campi in formato stampa
	 */
	@Override
	public String toString(){
		return "--- --- ---\n  cogn:"+cognome+"\n  nome:"+nome+"\n---\n  col:"+colore+"\n  per:"+permanente+"\n  tri:"+tricologico+"\n---\n  tel:"+telefono+"\n  via:"+via+"\n  cap:"+cap+"\n  pae:"+paese+"\n--- --- ---";
	}	
}