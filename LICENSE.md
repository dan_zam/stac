Copyright (c) 2013-2019, Daniele Zambon.
Copyright (c) 2002 and later by MH Software-Entwicklung. 
All Rights Reserved.

This file is part of Stac.

Stac is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Stac is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Stac.  If not, see <http://www.gnu.org/licenses/>.
