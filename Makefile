compile:
	javac src/*.java -cp JTattoo-1.6.7-sources

createjar:
	# jar --create --file stac.jar --manifest prep_files/MANIFEST.MF -C src . -C JTattoo-1.6.7-sources .
	jar cvfm stac.jar prep_files/MANIFEST.MF -C src . -C JTattoo-1.6.7-sources .

all: compile createjar

