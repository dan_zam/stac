#/bin/bash

DESKTOP_FOLDER=$(xdg-user-dir DESKTOP)
STAC_FOLDER="/home/$USER/stac"

STAC_VERSION=1.10
LICENSE_FILE=LICENSE.md
LICENSE_FILE_COMMENTED=lic_cmt.txt
STAC_LAUNCHER=stac
STAC_LAUNCHER_EM=stac-emergency
STAC_LAUNCHER_BASE=prep_files/stac_launcher.txt
STAC_LAUNCHER_DESKTOP="$STAC_LAUNCHER.desktop"
STAC_LAUNCHER_EM_DESKTOP="$STAC_LAUNCHER_EM.desktop"


# create tmp licence file text
echo "" > $LICENSE_FILE_COMMENTED
while IFS='' read -r line || [[ -n "$line" ]]; do
    echo "# $line" >> $LICENSE_FILE_COMMENTED
done < "$LICENSE_FILE"

# create stac
echo "#!/bin/bash" > $STAC_LAUNCHER
# echo "# this is an autogenerated script" >> $STAC_LAUNCHER
cat $LICENSE_FILE_COMMENTED >> $STAC_LAUNCHER
echo "\n" >> $STAC_LAUNCHER
echo "STAC_FOLDER=$STAC_FOLDER" >> $STAC_LAUNCHER
echo "DESKTOP_FOLDER=$DESKTOP_FOLDER" >> $STAC_LAUNCHER
cat $STAC_LAUNCHER_BASE >> $STAC_LAUNCHER
chmod +x $STAC_LAUNCHER

# create stac emergency
# echo "#/bin/bash" > $STAC_LAUNCHER_EM
# cat $LICENSE_FILE_COMMENTED >> $STAC_LAUNCHER_EM
# echo "./$STAC_LAUNCHER" >> $STAC_LAUNCHER_EM


# define path for saving database.
echo $DESKTOP_FOLDER > "./data/salvataggio-chiavetta.bst"
echo "201409050841AM" > "./data/uabu.bst"
echo "201409050841AM" > "./data/usa.bst"
echo "COGNOME~NOME~colore~permanente~tricologico~telefono~via~cap~paese" > "./data/st.bst"
echo "ROSSI~MARIO~~permanente MR~tri Mr~321456~seconda stella a destra~31415~pigreco" >> "./data/st.bst"
echo "BIANCHI~LUIGI~colore LB~permanente LB~tri lb~1414213~via eulero~21012~castino" >> "./data/st.bst"
echo "PAPERA~PAMELA~tutti i colori più uno~non credo l'abbia mai fatta; ora si~~~~~" >> "./data/st.bst"


# create .desktop
echo "[Desktop Entry]" > $STAC_LAUNCHER_DESKTOP
echo "Encoding=UTF-8" >> $STAC_LAUNCHER_DESKTOP
echo "Version=$STAC_VERSION" >> $STAC_LAUNCHER_DESKTOP
echo "Type=Application" >> $STAC_LAUNCHER_DESKTOP
echo "Terminal=false" >> $STAC_LAUNCHER_DESKTOP
echo "Exec=$STAC_FOLDER/$STAC_LAUNCHER" >> $STAC_LAUNCHER_DESKTOP
echo "Name=stac" >> $STAC_LAUNCHER_DESKTOP
echo "Icon=$STAC_FOLDER/images/stac.png" >> $STAC_LAUNCHER_DESKTOP

echo "[Desktop Entry]" > $STAC_LAUNCHER_EM_DESKTOP
echo "Encoding=UTF-8" >> $STAC_LAUNCHER_EM_DESKTOP
echo "Version=$STAC_VERSION" >> $STAC_LAUNCHER_EM_DESKTOP
echo "Type=Application" >> $STAC_LAUNCHER_EM_DESKTOP
echo "Terminal=false" >> $STAC_LAUNCHER_EM_DESKTOP
echo "Exec=$STAC_FOLDER/$STAC_LAUNCHER --emergency" >> $STAC_LAUNCHER_EM_DESKTOP
echo "Name=stac emergency" >> $STAC_LAUNCHER_EM_DESKTOP
echo "Icon=$STAC_FOLDER/images/stac-emergency.png" >> $STAC_LAUNCHER_EM_DESKTOP

# remove 
rm $LICENSE_FILE_COMMENTED
