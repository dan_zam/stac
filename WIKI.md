## data folder
* `sa.bst`: salvataggio automatico schede tecniche
* `usa.bst`: data ultimo salvataggio `sa.bst`
* `uabu.bst`: data dell'ultimo backup mensile automatico
* `abu-<data>.bst`: backup mensile automatico (ad esempio: `abu-201903070805AM.bst`)
* `scat.log`: file di log
* `salvataggio-chiavetta.bst`: dove salvare un backup delle schede tecniche da mettere, ad esempio, su chiavetta.

## Compiling and running from command line

What follows can be done at once with `make all`

Compile the stac source and JTattoo library
```bash
javac src/*.java -cp JTattoo-1.6.7-sources
```

You can run directly with
```bash
java -cp "./JTattoo-1.6.7-sources:./src" mainWin
```

or create an executable `.jar` and run that
```bash
jar --create --file stac.jar --manifest MANIFEST.MF -C src . -C JTattoo-1.6.7-sources .
java -jar stac.jar
```
where the file `MANIFEST.MF` is
```
Manifest-Version: 1.1
Class-Path: ./JTattoo-1.6.7-sources ./src
Main-Class: mainWin
```
