if [[ $* == *--emergency* ]]
then
    if [ -f $STAC_FOLDER/data/st.bst ]; then
        mv $STAC_FOLDER/data/st.bst $STAC_FOLDER/data/st.bst.bak
    fi
    if [ ! -f $DESKTOP_FOLDER/chiavetta.bst ]; then
        zenity --error --text="Non hai messo il file 'chiavetta.bst' sul Desktop\!" --title="STAC Emergency\!"
        exit 1
    fi
    cp $DESKTOP_FOLDER/chiavetta.bst $STAC_FOLDER/data/st.bst 
fi

cd $STAC_FOLDER
java -jar stac.jar