# Stac

Stac is a software to manage customer records and treatments. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This software requires a Java Runtime Environment to be installed, for example with the following command.

```bash
sudo apt install default-jre
```

### Install

* Go to your preferred folder, in which you intend to install Stac (I suggest to go in your `/home/$USER` folder).
* clone the project from git repository
```bash
git clone https://bitbucket.org/dan_zam/stac.git
cd stac
```
* If you preferred a folder different from `/home/$USER`, then you need to modify the first lines in `install.sh` accordingly.

* You can build it from source
```bash
make all
```
or recover a precompiled jar
```bash
cp stac.jar.precompiled stac.jar
```

* Setup the environment files
```bash
source install.sh
```
(There will be created also `.desktop` files that you can drag and drop in your desktop or toolbar.)

Now Stac should be properly installed.


### Running Stac

There are two ways of running Stac: the standard and emergency way. To run the standard it is sufficient to run
```bash
/path/to/installation/folder/stac
```

Differently, for the emergency mode, you need at first to place a proper Stac database file named `chiavetta.bst` in your Desktop folder, then run
```bash
/path/to/installation/folder/stac --emergency
```

## Built With

* JTattoo-1.6.7 by MH Software-Entwicklung


## Authors

* **Daniele Zambon**

## License

This project is licensed under the GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007 - see the [LICENSE.md](LICENSE.md) and [COPYING](COPYING) files for details.
