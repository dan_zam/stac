## Bugs
- [ ] a volte fa per chiudere ma non trova tipo il file per salvare (non ricordo come replicare l'evento)
- [x] fare che non si apre se non trova la chiavetta
- [x] check chiavetta su desktop per emergency

## Features
- [ ] set up a working environment on a Raspberry Pi
- [ ] aprire una sola istanza del programma o almeno avvertire che altre sono aperte.
- [ ] sarebbe bello che tutti file dell'utente fossero zippati tipo in un .odf

## Heavy
- [ ] become independent on JTattoo
- [ ] rewrite in python
- [ ] move to json
